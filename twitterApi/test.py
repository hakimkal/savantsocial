SOCIAL_AUTH_TWITTER_KEY = 'CBgHGDZr5eQMScky7gM7JL415'
SOCIAL_AUTH_TWITTER_SECRET = 'iv3cxuuOjR3H0Yp07RVLpx9KyVX83es2ZIP5Hx81UTRMqqOP5c'
OAUTH_TOKEN_SECRET = '5ON85x2g0APm3gVitNM8E7fHsXIa8WZeUm5sK5Eq7lFTn'
OAUTH_TOKEN = '2214166955-OKC7Lp1TyCsqveXht2P8AT4vUzkOwjMZUsDkLaV'

import string
import requests
import json
from requests_oauthlib import OAuth1
import tweepy
from tweepy import OAuthHandler
import datetime
from collections import Counter
# from social_django.utils import load_strategy
import time
# from django.conf import  settings
# from .models import Mentions
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords

# Just a function to extract the actual location from the user data
def extract_location(location):
    tokenizer = TweetTokenizer()
    punct = list(string.punctuation)
    stopwords_list = stopwords.words('english') + punct + ['rt', 'via', '...']
    
    text = location.lower()
    tokens = tokenizer.tokenize(text)
    return [tok for tok in tokens if tok not in stopwords_list and not tok.isdigit()]    

def extract_location_bad(location):
    if location != '':
        location = location.lower()
        location = location[:location.find(',')+1]
        location = location[:location.find(' ')]
        if location.strip() != '':
            return location

def get_token(user, provider):
    if (provider == 'twitter'):
        social = user.social_auth.get(provider=provider)
        if (social.extra_data['auth_time']) <= int(time.time()):
            strategy = load_strategy()
            social.refresh_token(strategy)
    else:
        social = user.social_auth.get(provider=provider)
        if (social.extra_data['auth_time'] + social.extra_data['expires']) <= int(time.time()):
            strategy = load_strategy()
            social.refresh_token(strategy)
    return social.extra_data['access_token']


def extract_tweets_date_wrapper(tweets):
    created_at  = list(map(lambda t: t['created_at'], tweets))
    dates = list(map(lambda t: t[:t.find('+')], created_at))
    return dates

def extract_retweets_day(tweets):
    days = list(map(lambda t: t['created_at'][0:2], tweets))
    retweets = list(map(lambda t: t['retweet_count'], tweets))
    # print(days)

    days_pos = [["Mon", 0], ["Tue", 0], ["Wed", 0], ["Thu", 0], ["Fri", 0], ["Sat", 0], ["Sun", 0]]

    for i in range(0, len(days)):
        for j in range(0, len(days_pos)):
            if days[i] in days_pos[j][0]:
                days_pos[j][1] = days_pos[j][1]+retweets[i]
    
    new_days = list(map(lambda t: t[0], days_pos))
    new_retweets = list(map(lambda t: t[1], days_pos))

    return new_days, new_retweets
    

def extract_tweet_likes(tweets):
    days = list(map(lambda t: t['created_at'][0:2], tweets))
    likes = list(map(lambda t: t['favorite_count'], tweets))
    
    days_pos = [["Mon", 0], ["Tue", 0], ["Wed", 0], ["Thu", 0], ["Fri", 0], ["Sat", 0], ["Sun", 0]]

    for i in range(0, len(days)):
        for j in range(0, len(days_pos)):
            if days[i] in days_pos[j][0]:
                days_pos[j][1] = days_pos[j][1]+likes[i]
    
    new_days = list(map(lambda t: t[0], days_pos))
    new_likes = list(map(lambda t: t[1], days_pos))

    return new_days, new_likes

def twitter_engagement_wrapper(tweets, count):
    engagements = [];
    if(count != 0):
        engagements = list(map(lambda t: round(((t['retweet_count'] + t['favorite_count']) / int(count)) * 100, 2), tweets))
    return engagements

def twitter_impression_wrapper(tweets):
    impression = list(map(lambda t: t['retweet_count'] + t['favorite_count'], tweets))
    return impression

def retweets_wrapper(tweets):
    pass


class TweetStreamer(tweepy.StreamListener):
    def __init__(self, user):
        tweepy.StreamListener.__init__(self)
        self.user = user
        
    def on_status(self, status):
        username = status.user.screen_name
        status_id = status.id
        retweeted = status.retweet_count
        tweet = status.text
        save_data = Mentions(
            user = self.user,
            screen_name  = username,
            tweet_text = tweet,
            retweets_count = retweeted,
            status_id = status_id
        )
        save_data.save()

class TwiiterClient(object):
    
    def __init__(self):
        '''
        Class constructor or initialization method.
        '''
        # keys and tokens from the Twitter Dev Console
        consumer_key = SOCIAL_AUTH_TWITTER_KEY

        consumer_secret = SOCIAL_AUTH_TWITTER_SECRET
        token = OAUTH_TOKEN
        token_secret = OAUTH_TOKEN_SECRET
 
        # attempt authentication
        try:
            # create OAuthHandler object
            self.auth = OAuthHandler(consumer_key, consumer_secret)
            # set access token and secret
            self.auth.set_access_token(token, token_secret)
            # create tweepy API object to fetch tweets
            self.api = tweepy.API(self.auth)
        except:
            pass
    def get_tweets(self):
        # Main function to get tweets
        tweets = []

        try:
            fetched_tweets = self.api.user_timeline(count=500, include_rts= True)
            if len(fetched_tweets) > 0:
                for status in fetched_tweets:
                    tweets.append(status._json)
                return reversed(tweets)
            else:
                return []

        except tweepy.TweepError as e:
            return e
    
    def search_users(self, query):
        try: 
            users = []
            users = self.api.search_users(q = query, per_page = 20, page = 1)
            main_user = users[0]
            del users[0]
            return users, main_user
        except (tweepy.TweepError, IndexError, KeyError, TypeError) as e:
            return e

    def search_tweets(self, q):
        try:
            tweets = []
            for page in tweepy.Cursor(self.api.search, q).pages():
                for tweet in page:
                    yield tweet
        except tweepy.TweepError as e:
            print('Tweepy error {}'.format(e))


    def stream_twitter(self,screen_name, user):
        myStreamListener = TweetStreamer(user)
        stream = tweepy.Stream(self.api.auth, myStreamListener)
        stream.filter(track=[screen_name], async=True)


    def get_trends(self):
        trends = []
        try:
            trends = self.api.trends_place(id = 23424908)
            trends = [t for t in trends[0]['trends'] if t['tweet_volume'] != None ]
            return trends
        except tweepy.TweepError as e:
            return e

    def get_followers_count(self):

        follower_count = 0

        try:
            fetched_counts = self.api.me()
            follower_count = fetched_counts.followers_count
            return follower_count

        except tweepy.TweepError as e:
            return e

    
    def get_followers_data(self, id = ""):
        data = []
        try:
            if(id == ""):
                ids = self.api.followers_ids()
            else:
                ids = self.api.followers_ids(id = id)
            for id in reversed(ids):
                data = self.api.get_user(id)
                yield data
        except tweepy.TweepError as e:
            pass

            
    def get_user_data(self, query):
        try:
            user = self.api.get_user(id = query)
            return user
        except tweepy.TweepError as e:
            return e

    def get_user_status(self):
        try:
            for mentions in tweepy.Cursor(self.api.mentions_timeline).items():
                yield mentions
        except tweepy.TweepError as e:
            pass
    
    def post_tweets(self, status, username, status_id):
        try:
            if(username):
                response = self.api.update_status('@{0} {1}'.format(username, status), in_reply_to_status_id=status_id)
                return ({"status":True, "message": 'Sucessfully sent'})
            else:
                response =self.api.update_status(status)
                return ({"status":True, "message": 'Sucessfully sent'})

        except Exception as e:
            return ({"status": False, "message":'Unable to Post Tweet', "error": e})

    def post_tweets_with_images(self, status, files, username, status_id):
        try:
            if(username):
                status = self.api.update_with_media(files.path, '@{0} {1}'.format(username, status), in_reply_to_status_id=status_id)
                return ({"status":True, "message":'Successfully sent'})
            else:
                status = self.api.update_with_media(files.path, status)
                return ({"status":True, "message": 'Successfully Sent'})
        except tweepy.TweepError as e:
            return ({"status": False, "message":'Unable to Post Tweet', "error": e})


    def post_tweet_reply(self, status, files, username, status_id):
        try:
            if files is not None:
                print(files)
                response = self.post_tweets_with_images(status = status, status_id = None, files = files, username = False)
                return response
            else:
                print(status)
                response = self.post_tweets(status = status, status_id = None, username = False)
                return response
        except tweepy.TweepError as e:
            return ({"status": False, "message": 'Unable to post tweet', "error": e})
import folium

def map_tweets(tweets):
    geo_data = {
        "type": "FeatureCollection",
        "features":[]
    }

    for tweet in tweets:
        try:
            geo_json_feature = {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": tweet['coordinates']['coordinates']
                },
                "properties": {
                    "text": tweet['text'],
                    "created_at": tweet['created_at']
                }
            }
            geo_data['features'].append(geo_json_feature)
            return geo_data
        except KeyError:
            pass
        

def make_map(geo_data, map_file):
    sample_map = folium.Map(location=[50, 5], zoom_start=5)
    geojson_layer = folium.GeoJson(geo_data, name='geojson')
    geojson_layer.add_to(sample_map)
    sample_map.save(map_file)

if __name__== '__main__':
    api = TwiiterClient()
    tf = Counter()
    for f in api.get_followers_data():
        tokens = extract_location(location=f.location)
        tf.update(tokens)
    for tag, count in tf.most_common(20):
        print("{}: {}".format(tag, count))
