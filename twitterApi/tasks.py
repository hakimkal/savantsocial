from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .utils import get_token
from .utils import TwiiterClient
from .models import FollowersData, Tweet, FollowerCount, Post
from django.contrib.auth.models import User
import logging
import datetime
from savantsocial.celery import app
from tweepy import RateLimitError
from savantsocial.config import huey
from huey import crontab
from huey.contrib.djhuey import periodic_task, db_task, task
import time
import re
now  = datetime.datetime.now()

# @app.task(max_retries=5, bind=True, name="Fetch-Twitter-Data", default_retry_delay=30 * 60)

@periodic_task(crontab(minute='*/10'))
def stream_tweets_periodically():
    try:
        users = User.objects.all()
        for profile in users:
            if profile.social_auth.filter(provider = 'twitter'):
                access_token = get_token(profile, provider = 'twitter')
                social = profile.social_auth.get(provider='twitter')
                screen_name = social.extra_data['access_token']['screen_name']
                api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
                stream = api.stream_twitter(screen_name = screen_name,user= profile)
    except (Exception, RateLimitError) as exc:
        logging.warning("The user might not exists %s" %exc)




@task()
def get_competitors_data(user_id, competitor_twitter_id):
    try:
        user_data = User.objects.get(id = user_id)
        competitor_data = user_data.competitor_set.get(twitter_id = competitor_twitter_id)
        if user_data.social_auth.filter(provider = 'twitter'):
            access_token = get_token(user_data, provider = 'twitter')
            api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
            count = api.get_user_data(query = competitor_twitter_id)
            for user in api.get_followers_data(id = competitor_twitter_id,followers_count=count.followers_count):
                if (FollowersData.objects.filter(user = user_data, competitor= competitor_data, follower_id = user.id_str).count() > 0):
                    return 'We already have this data'
                else:
                    save_data, created = FollowersData.objects.update_or_create(
                        user = user_data,
                        competitor = competitor_data,
                        follower_id = user.id_str,
                        name = user.name,
                        screen_name = user.screen_name,
                        location = user.location,
                        verified = user.verified,
                        folowers_count = user.followers_count,
                        friends_count = user.friends_count,
                        listed_count = user.listed_count,
                        tweets_count = user.statuses_count,
                        profile_image_url = user.profile_image_url,
                    )
    except (Exception, RateLimitError) as exc:
        logging.warning("The user might not exists %s" %exc)


@periodic_task(crontab(minute='*/2'))
def competitors_data_periodically():
    try:
        users = User.objects.all()
        for profile in users:
            if profile.social_auth.filter(provider = 'twitter'):
                access_token = get_token(profile, provider = 'twitter')
                api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
                competitors = profile.competitor_set.all()
                if len(competitors) > 0:
                    for competitor in competitors:
                        count = api.get_user_data(query = competitor.twitter_id)
                        for user in api.get_followers_data(id=competitor.twitter_id ,followers_count=count.followers_count):
                            if (FollowersData.objects.filter(user = profile,competitor= competitor , follower_id = user.id_str).count() > 0):
                                return 'We already have this data'
                            else:
                                save_data, created = FollowersData.objects.update_or_create(
                                    user = profile,
                                    competitor = competitor,
                                    follower_id = user.id_str,
                                    name = user.name,
                                    screen_name = user.screen_name,
                                    location = user.location,
                                    verified = user.verified,
                                    folowers_count = user.followers_count,
                                    friends_count = user.friends_count,
                                    listed_count = user.listed_count,
                                    tweets_count = user.statuses_count,
                                    profile_image_url = user.profile_image_url,
                                )
    except (Exception, RateLimitError) as exc:
        logging.warning("The user might not exists %s" %exc)


@task()
def save_data(user_id):
    try:
        user_data = User.objects.get(user__id = user_id)
        if user_data.social_auth.filter(provider = 'twitter'):
            access_token = get_token(user_data, provider = 'twitter')
            api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
            count = FollowerCount.objects.first()
            for user in api.get_followers_data(followers_count=count.count):
                if (FollowersData.objects.filter(user = user_data, follower_id = user.id_str).count() > 0):
                    return 'We already have this data'
                else:
                    save_data, created = FollowersData.objects.update_or_create(
                        user = user_data,
                        follower_id = user.id_str,
                        name = user.name,
                        screen_name = user.screen_name,
                        location = user.location,
                        verified = user.verified,
                        folowers_count = user.followers_count,
                        friends_count = user.friends_count,
                        listed_count = user.listed_count,
                        tweets_count = user.statuses_count,
                        profile_image_url = user.profile_image_url,
                    )
    except (Exception, RateLimitError) as exc:
        logging.warning("The user might not exists %s" %exc)


@periodic_task(crontab(minute='*/1'))
def save_twitter_data():
    try:
        users = User.objects.all()
        for profile in users:
            if profile.social_auth.filter(provider = 'twitter'):
                access_token = get_token(profile, provider = 'twitter')
                api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
                count = FollowerCount.objects.first()
                for user in api.get_followers_data(followers_count=count.count):
                    if (FollowersData.objects.filter(user = profile, follower_id = user.id_str).count() > 0):
                        return 'We already have this data'
                    else:
                        save_data, created = FollowersData.objects.update_or_create(
                            user = profile,
                            follower_id = user.id_str,
                            name = user.name,
                            screen_name = user.screen_name,
                            location = user.location,
                            verified = user.verified,
                            folowers_count = user.followers_count,
                            friends_count = user.friends_count,
                            listed_count = user.listed_count,
                            tweets_count = user.statuses_count,
                            profile_image_url = user.profile_image_url,
                        )
    except (Exception, RateLimitError) as exc:
        logging.warning("The user might not exists %s" %exc)

# @app.task(max_retries=5, bind=True, name="Fetch-Tweets-Data", default_retry_delay=30 * 60)

@periodic_task(crontab(minute='*/1'))
def save_tweets_data():
    try:
        users = User.objects.all()
        for profile in users:
            if profile.social_auth.filter(provider = 'twitter'):
                access_token = get_token(profile, provider = 'twitter')
                api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
                tweets = api.get_tweets() 
                if tweets is not None:
                    for tweet in tweets:
                        if(tweet is not None):
                            if(Tweet.objects.filter(user= profile, tweet_id = tweet['id_str']).count() > 0):
                                return 'This data already exists'
                            else:
                                Tweet.objects.update_or_create(
                                    user = profile,
                                    tweet_id = tweet['id_str'],
                                    retweeted = tweet['retweeted'],
                                    retweet_count = tweet['retweet_count'],
                                    text = tweet['text'],
                                    location = tweet['geo'],
                                    created_at = tweet['created_at'],
                                    favorite_count = tweet['favorite_count']
                                )
                else: 
                    return 'Could not fetch any tweets'
    except (Exception, RateLimitError) as exc:
        logging.warning("Some error just occured %s" %exc)


# @app.task(max_retries=5, bind=True, name="Fetch-Followers-Data", default_retry_delay=30 * 60)

@periodic_task(crontab(minute='*/1'))
def fetch_follower_count():
    try:
        users  = User.objects.all()
        for profile in users:
            if profile.social_auth.filter(provider = 'twitter'):
                access_token = get_token(profile, provider = 'twitter')
                api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
                followers_count = api.get_followers_count()           
                if FollowerCount.objects.count() > 0:
                    followers_for_count_today = FollowerCount.objects.filter(
                            day__day= now.day,
                            day__month = now.month,
                            day__year = now.year,
                            user = profile
                        )
                    followers_count = 0
                    if(followers_for_count_today is not None ):
                        return "we already got for today"
                    else:
                        save_count = FollowerCount(user = profile, count=followers_count)
                        save_count.save()
                else:
                        save_count = FollowerCount(user = profile, count=followers_count)
                        save_count.save()

    except (Exception, RateLimitError) as exc:
        logging.warning("Some error just occured %s" %exc)




def isItTime(time): #receives time in the form XXXX-XX-XX XX:XX:XX per day
    times = []
    postFlag = False
    # Determining if it is the correct time:
    nowStr = now.strftime("%H:%M:%S") #Converts datetime object to string (so that it can be converted to a time object below)
    nowTime = datetime.datetime.time(datetime.datetime.now())
    diffTime = datetime.datetime.combine(datetime.date.min, nowTime) - datetime.datetime.combine(datetime.date.min, time)
    diffTimeSeconds = diffTime.total_seconds()
    print(diffTimeSeconds)
    if diffTimeSeconds>=0 and diffTimeSeconds<60:
        postFlag = True
        print('It\'s the right time')

    return postFlag


@periodic_task(crontab(minute='*/1'))
def post_scheduled_tweets():
    if Post.objects.count() > 0:
        posts = Post.objects.filter(
            schedule__day = now.day,
            schedule__month = now.month,
            schedule__year = now.year,
            scheduled = True
        )
        if len(posts) > 0:
            for post in posts:
                time = isItTime(post.schedule.time())
                if time:
                    access_token = get_token(post.user, provider = 'twitter')
                    api = TwiiterClient(access_token = access_token['oauth_token'], access_token_secret = access_token['oauth_token_secret'])
                    if len(post.image.name) > 0:
                        status = api.post_tweets_with_images(status = post.text, username = False, status_id=None, files = post.image)
                    else:
                        status = api.post_tweets(status = post.text, username = False, status_id = None)
                    if status == True:
                        post.scheduled = True
                        post.save()
                        print('Completed')
                    return ({'message': 'completed'})
        else: 
            print('No post exists')
    else:
        print('No post exists')
