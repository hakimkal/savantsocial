# Generated by Django 2.0.2 on 2018-03-13 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitterApi', '0012_auto_20180313_1238'),
    ]

    operations = [
        migrations.AlterField(
            model_name='followercount',
            name='count',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
