from django.test import TestCase
from .models import FollowersData, FollowerCount
from rest_framework.test import APIClient
from rest_framework import status
# from django.core.urlresolvers import reverse
import datetime
# Create your tests here.


class ModelTestCase(TestCase):
    """The class defines the test suite for the FollowersData model"""

    def setUp(self):
        #defines the test client and other test variables
        self.followersData_follower_id = "123456"
        self.followersData_name = "Test Subject"
        self.followersData_screen_name = "TestSeubject"
        self.followersData_location = "Abuja"
        self.followersData_verified = False
        self.followersData_followers_count = "233"
        self.followersData_friends_count = "12"
        self.followersData_listed_count = "1222"
        self.followersData_tweets_count = "122"
        self.followersData_profile_image_url = "http://example.com"

        self.followersData = FollowersData(
            follower_id = self.followersData_follower_id,
            name = self.followersData_name,
            screen_name = self.followersData_screen_name,
            location = self.followersData_location,
            verified = self.followersData_verified,
            folowers_count = self.followersData_followers_count,
            friends_count = self.followersData_friends_count,
            listed_count = self.followersData_listed_count,
            tweets_count = self.followersData_tweets_count,
            profile_image_url = self.followersData_profile_image_url,
            )

    def test_model_can_create_followers_data(self):
        #Test to see if the bucketlist can create FollowerData
        old_count = FollowersData.objects.count()
        self.followersData.save()
        new_count = FollowersData.objects.count()
        self.assertNotEqual(old_count, new_count)


# # Define this after the ModelTestCase
# class ViewTestCase(TestCase):
#     """Test suite for the api views."""

#     def setUp(self):
#         """Define the test client and other test variables."""
#         self.client = APIClient()
#         self.bucketlist_data = {'name': 'Go to Ibiza'}
#         self.response = self.client.post(
#             reverse('create'),
#             self.bucketlist_data,
#             format="json")

#     def test_api_can_create_a_bucketlist(self):
#         """Test the api has bucket creation capability."""
#         self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)