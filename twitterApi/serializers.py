from rest_framework import serializers
from .models import FollowerCount, FollowersData

class FollowerCountSerializer(serializers.ModelSerializer):

    class Meta:
        model = FollowerCount
        fields = ('id', 'day', 'count')
        # read_only_fields = ('day')


class FollowerDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = FollowersData
        fields = (
            'follower_id',
           'name',
            'screen_name',
            'location',
            'verified',
            'folowers_count',
            'friends_count',
            'listed_count',
            'tweets_count',
            'profile_image_url'
        )
    