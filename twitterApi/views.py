from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponse
import csv
from rest_framework import generics

from django.forms.models import model_to_dict
from .serializers import FollowerCountSerializer, FollowerDataSerializer
from .models import FollowerCount, FollowersData, Tweet, Post, Mentions
from django.utils import timezone, timesince
import datetime
import time
from social_django.utils import load_strategy
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from collections import Counter
from django.contrib.auth.decorators import login_required
from accounts.forms import CompetitorTwitterForm
from django.db.models import Max
import json
from .sentiment import get_tweet_sentiment
from django.contrib.auth.decorators import login_required
from .utils import (
    extract_location,
    twitter_engagement_wrapper,
    TwiiterClient,
    extract_tweets_date_wrapper,
    twitter_impression_wrapper,
    extract_retweets_day,
    extract_tweet_likes,
    get_total_impressions,
    convert_by_length
)
from django.http import JsonResponse
from .forms import SearchForm, PostForm
import json

now = datetime.datetime.now()
# Create your views here.


@login_required()
def follower_data(request):
    if request.user.social_auth.filter(provider = 'twitter'):
        access_token = get_token(request.user, provider='twitter')
        api = TwiiterClient(
            access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
        template = 'twitter/twitter.html'
        followers_location = FollowersData.objects.filter(
            user=request.user).values_list('location', flat=True)
        locations = []
        if len(followers_location) > 0:
            for location in followers_location:
                locations.append(extract_location(location))
        locations = Counter(locations)

        followers_count = api.get_followers_count()
        # fetch followers count for the day
        count_for_today = FollowerCount.objects.filter(
            day__day = now.day,
            day__month = now.month,
            day__year = now.year,
            user__pk = request.user.id
        ).count()
        if count_for_today > 0:
            print('we already have this data')
        else:
            save_count = FollowerCount(user = request.user, count=followers_count)
            save_count.save()

        
        follower_count = FollowerCount.objects.filter(
            user__pk=request.user.id).values('count', 'day')

        counts = []
        if len(follower_count) > 0:
            for count in follower_count:
                counts.append(count)
        tweets = Tweet.objects.filter(user=request.user).values(
            'favorite_count', 'retweet_count', 'created_at')
        if(FollowerCount.objects.filter(user=request.user, day__day=now.day).count()) > 0:
            follower_count = FollowerCount.objects.filter(
                user=request.user,
                day__day=now.day
            )[0]
        else:
            follower_count = FollowerCount()
            follower_count.count = 0
        engagements = twitter_engagement_wrapper(tweets, followers_count)
        impressions = twitter_impression_wrapper(tweets)
        tweet_dates = extract_tweets_date_wrapper(tweets)
        retweet_days, retweet_counts = extract_retweets_day(tweets)
        like_days, like_count = extract_tweet_likes(tweets)
        context_data = {
            'retrieved_followers': FollowersData.objects.filter(user=request.user, competitor=None).count(),
            'current_followers': followers_count,
            'followers_location': locations,
            'followers_count': counts,
            'tweet_dates': list(reversed(tweet_dates)),
            'engagements': list(reversed(engagements)),
            'impressions': list(reversed(impressions)),
            'retweet_days': list(reversed(retweet_days)),
            'retweet_counts': list(reversed(retweet_counts)),
            'like_days': list(reversed(like_days)),
            'like_count': list(reversed(like_count)),
            'form': SearchForm(),
            'post_form': PostForm()
        }
        return render(request, template, context_data)
    else:
        messages.warning(request, 'Please connect your twitter account to the app to view analytics')
        return redirect('accounts:home')


@login_required()
def tweets(request):
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(
        access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    tweets = api.get_tweets()
    for tweet in tweets:
        save_data = Tweet(
            user=request.user,
            tweet_id=tweet['id_str'],
            retweeted=tweet['retweeted'],
            retweet_count=tweet['retweet_count'],
            text=tweet['text'],
            location=tweet['geo'],
            created_at=tweet['created_at'],
            favorite_count=tweet['favorite_count']
        )
        save_data.save()
    return JsonResponse(tweets, safe=False)


def get_token(user, provider):
    if (provider == 'twitter'):
        social = user.social_auth.get(provider=provider)
        if (social.extra_data['auth_time']) <= int(time.time()):
            strategy = load_strategy()
            social.refresh_token(strategy)
    else:
        social = user.social_auth.get(provider=provider)
        if (social.extra_data['auth_time'] + social.extra_data['expires']) <= int(time.time()):
            strategy = load_strategy()
            social.refresh_token(strategy)
    return social.extra_data['access_token']


@login_required()
def search_twitter(request):
    if request.method == 'POST':
        access_token = get_token(request.user, provider='twitter')
        api = TwiiterClient(
            access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
        form = SearchForm(request.POST)
        if form.is_valid():
            s_query = form.cleaned_data['search']
            try:
                result, main_user = api.search_users(query=s_query)
                # print(main_user.status[])
                competitor_form = CompetitorTwitterForm(initial={
                    'name': main_user.name,
                    'description': main_user.description,
                    'website': main_user.url,
                    'twitter_id': main_user.id_str
                })
                return render(request, 'twitter/twitter-results.html', {'form': form,
                                                                        'results': result,
                                                                        'main_user': main_user,
                                                                        'competitor_form': competitor_form
                                                                        })
            except (TypeError, IndexError, IndentationError) as e:
                messages.warning(
                    request, 'The search query did not match any twitter user')
    else:
        form = SearchForm()

    return render(request, 'twitter/twitter-results.html', {'form': form})


@login_required()
def trends(request):
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(
        access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    template_name = "twitter/results-twitter.html"
    trends = api.get_trends()
    return render(request, template_name, {'trends': trends})


@login_required()
def export_analysis_to_twitter(request):
    response = HttpResponse(content='text/csv')
    response['Content-disposition'] = 'attachment; filename="followers_data.csv"'
    writer = csv.writer(response)
    writer.writerow(['locations', 'follower_count', 'tweet_dates',
                     'tweet_engagements', 'tweet_impressions', 'retweets', 'likes'])
    followers_data = FollowersData.objects.filter(user=request.user).values_list(
        'follower_id', 'name', 'screen_name', 'location', 'folowers_count', 'friends_count')

    for data in followers_data:
        writer.writerow(data)
    return response


@login_required()
def user_status(request):
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(
        access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    mentions = []
    for status in api.get_user_status():
        mentions.append(status._json)
    return JsonResponse(mentions, safe=False)


@login_required()
def sentiment_analyser(request):
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(
        access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    tweets = api.search_tweets(q='gtbank')
    analysed = []
    if tweets is not None:
        for tweet in tweets:
            sent = get_tweet_sentiment(tweet.text)
            analysed.append({tweet.text: sent})
    return JsonResponse(analysed, safe=False)


@login_required()
def stream_tweets(request):
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    social = request.user.social_auth.get(provider='twitter')
    screen_name = social.extra_data['access_token']['screen_name']
    stream = api.stream_twitter(screen_name = screen_name,user= request.user)
    mention = Mentions.objects.filter(user=request.user).count()
    return JsonResponse({'data': mention}, safe=False)


@login_required()
def post_tweet(request):
    template_name = 'twitter/test.html'
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(
        access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    form = PostForm()
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            tweet = form.cleaned_data['text']
            try:
                post = form.save(commit=False)
                post.user = request.user
                new_post = post.save()
                new_post = Post.objects.get(id=post.id)
                if post.scheduled == False:
                    if len(new_post.image.name) > 0:
                        status = api.post_tweets_with_images(
                            status=new_post.text, files=new_post.image, username=False, status_id=None)
                    else:
                        status = api.post_tweets(
                            status=new_post.text, username=False, status_id=None)
                    print(status)
                    return redirect('search-twitter')

            except Exception as e:
                return messages.warning(request, e)

    return render(request, template_name, {'form': form})


@login_required()
def overview(request):
    template_name = 'twitter/overview.html'
    total_tweets = ''
    tweet_impressions = ''
    mentions = ''
    followers = []
    top_tweet = []
    top_mention = []
    try:
        access_token = get_token(request.user, provider='twitter')
        api = TwiiterClient(
            access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
        tweets = api.get_tweets()
        total_tweets = len(tweets)
        top_tweet = max(tweets, key=lambda x: x['retweet_count'])
        tweet_impressions = get_total_impressions(tweets)
        followers = FollowersData.objects.filter(user=request.user)
        followers_count = api.get_followers_count()
        for mention in api.get_user_status():
            top_mention.append(mention._json)
        mentions = len(top_mention)
        tp_mention = max(top_mention, key=lambda x: x['retweet_count'])
        top_follower = max(int(x.folowers_count) for x in followers)
        tp_follower = FollowersData.objects.get(folowers_count=top_follower)
        tp_follower = api.get_user_data(tp_follower.follower_id)
        return render(request, template_name, {
            'total_tweets': convert_by_length(total_tweets),
            'total_followers': convert_by_length(followers_count),
            'top_tweets': top_tweet,
            'top_mention': tp_mention,
            'tweet_impressions': convert_by_length(tweet_impressions),
            'total_mentions':convert_by_length(mentions),
            'top_follower': tp_follower
        })
    except Exception as e:
        messages.warning(request, 'Please try to reload the page')
    return render(request, template_name, {})

@login_required()
def twitter_mentions(request):
    template_name = 'twitter/mentions.html'
    mentions = Mentions.objects.filter(user = request.user)
    count = mentions.count()
    mentions = mentions[:50]
    return render(request, template_name, {'mentions':mentions, 'count': count})
