from django.contrib import admin
from .models import FollowerCount, FollowersData,Tweet, SearchHistory, Post, Mentions
# Register your models here.


admin.site.register(FollowerCount)
admin.site.register(FollowersData)
admin.site.register(Tweet)
admin.site.register(Post)
admin.site.register(SearchHistory)
admin.site.register(Mentions)
