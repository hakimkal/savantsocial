from django import forms
from .models import Post

class SearchForm(forms.Form):
    search = forms.CharField(max_length=50,
    widget= forms.TextInput(attrs={'class': ['form-control', 'form-control-lg']})
    )


class PostForm(forms.ModelForm):
    class Meta: 
        model = Post
        fields = ['text', 'schedule', 'image', 'scheduled']