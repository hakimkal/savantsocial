from django.db import models
from django.contrib.auth.models import User
from accounts.models import Competitor
import datetime

class FollowerCount(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)
    competitor = models.ForeignKey(Competitor, on_delete=models.CASCADE, blank=True, null=True)
    day = models.DateField(auto_now_add=True)
    count = models.IntegerField(default=0,null=True)

    def __str__(self):
        return "{}".format(self.count)


class FollowersData(models.Model):
    user = models.name = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)
    competitor = models.ForeignKey(Competitor, on_delete=models.CASCADE, blank=True, null=True)
    follower_id = models.CharField(max_length=255, null=False, blank=False)
    day = models.DateField(auto_now_add=True)
    name = models.CharField(blank=True, null=True, max_length=255)
    screen_name = models.CharField(blank=False, null=False, max_length=255)
    location = models.CharField(blank=True, max_length=255)
    verified = models.BooleanField()
    folowers_count = models.CharField(default=0, null=True, blank=True, max_length=255)
    friends_count = models.CharField(blank=True, null=True, max_length=255)
    listed_count = models.CharField(blank=True, null=True, max_length=255)
    tweets_count = models.CharField(blank=True, null=True, max_length=255)
    profile_image_url = models.CharField(blank=True, null=True, max_length=255)

    def __str__(self):
        return "{}".format(self.screen_name)


class Tweet(models.Model):
    user = models.name = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)
    competitor = models.ForeignKey(Competitor, on_delete = models.CASCADE, blank=True, null=True)
    tweet_id = models.CharField(max_length=255, null=False, blank=False)
    retweeted = models.BooleanField(default = False)
    retweet_count = models.IntegerField(blank=True, null=True)
    text = models.CharField(blank=False, null=False, max_length=255)
    location = models.CharField(blank=True, null = True, max_length=255)
    created_at = models.CharField(blank=True, null=True, max_length = 255)
    favorite_count = models.IntegerField(blank=False)

    def __str__(self):
        return "{}".format(self.text)


class SearchHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)
    query = models.CharField(blank=True, null=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    result_id = models.CharField(blank=True, null=True, max_length=255)
    
    def __str__(self):
        return "{}".format(self.query)

def user_directory_path(instance, filename):
    return 'tweets/user_{0}/{1}'.format(instance.user.id, filename)

class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    schedule = models.DateTimeField(default= datetime.datetime.now, blank=True, null=True)
    text = models.TextField()
    image = models.ImageField(upload_to= user_directory_path, blank=True, null=True)
    scheduled = models.BooleanField(default=False, blank=False, null=False)

    def __str__(self):
        return "{}".format(self.text)

class Mentions(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    screen_name = models.CharField(blank=True, null=True, max_length=255)
    name = models.CharField(blank=True, null=True, max_length=255)
    location = models.CharField(blank=True, null=True, max_length=255)
    profile_image_url = models.CharField(blank=True, null=True, max_length=255)
    verified = models.BooleanField(default = False)
    tweet_text = models.CharField(blank=True, null=True, max_length=255)
    retweets_count = models.CharField(blank=True, null=True, max_length=255)
    status_id = models.CharField(blank=True, null=True, max_length=255)

    
    def __str__(self):
        return "{} by {}".format(self.tweet_text, self.screen_name)