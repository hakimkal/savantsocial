import re
from textblob import TextBlob


def clean_tweet(tweet):
    """[A function to clean the incoming tweets by removing the hashtags and other stuff]
    
    Arguments:
        tweet {[string]} -- [The tweet text that needs to be cleaned]
    
    Returns:
        [string] -- [The cleaned tweet that has been filtered using regex]
    """

    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])(\w+:\/\/\S+)", " ", tweet).split())


def get_tweet_sentiment(tweet):
    """[A function to analyse the and extract the sentiment from tweets]
    
    Arguments:
        tweet {[string]} -- [The tweet text to be analysed]
    """


    # create a textblob object of the passed tweet text
    analysis = TextBlob(clean_tweet(tweet))

    # extract sentiment
    if analysis.sentiment.polarity < 0:     # Negative tweets
        sentiment =  {'decision':'Negative', 'value': analysis.sentiment.polarity}
    
    elif analysis.sentiment.polarity == 0:  # Neutral Tweets
        sentiment = {'decision':'Neutral', 'value': analysis.sentiment.polarity}
    
    else:                                   # Positive Tweets
        sentiment = {'decision':'Positive', 'value': analysis.sentiment.polarity}
    
    return sentiment
