from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (
        tweets,
        follower_data, 
        search_twitter, 
        trends, 
        export_analysis_to_twitter,
        user_status,
        sentiment_analyser,
        post_tweet,
        stream_tweets, 
        overview,
        twitter_mentions
            )
urlpatterns = {
     path('tweets/', tweets, name='tweets' ),
     path('post/', post_tweet, name='post-tweets' ),
     path('mentions/', twitter_mentions, name='twitter-mentions'),
     path('search/', search_twitter, name='search-twitter'),
     path('trends/', trends, name='trends-twitter'),
    path('followers/data/', follower_data, name='twitter_followers_data'),
    path('status/json', overview, name='user_status' ),
    path('stream/json', stream_tweets, name='stream_twitter' ),
    path('export/csv/', export_analysis_to_twitter, name='export_users_csv'),
}

urlpatterns = format_suffix_patterns(urlpatterns)