from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import posts, analytics

urlpatterns = {
     path('posts/', posts, name='tweets' ),
    path('analytics/', analytics, name='instagram_followers_data'),
    # path('analytics/json/', analyticsClass.as_view(), name='follwers_data_json'),
}

urlpatterns = format_suffix_patterns(urlpatterns)