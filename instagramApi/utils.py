import datetime
import time
import requests
import json
from collections import Counter
from instagram.client import InstagramAPI
from instagram.bind import InstagramAPIError
import requests
# from social_django.utils import load_strategy

BACK_UP_CLIENT = '8593252.c09ec1a.83deea9350bf4bb39f82c5937c86e56b'
CLIENT_SECRET = 'f76bf798d0c34fe9b42baf84b067609b'
PERSONAL = '349813513.ef959ed.cc427510c5d64cd790fbeb055325433d'
class instagramClient(object):
    def __init__(self, ACCESS_TOKEN):
        self.access_token = ACCESS_TOKEN
        try:
            self.api = InstagramAPI(access_token = self.access_token, client_secret = CLIENT_SECRET)
        except InstagramAPIError as e:
            pass
    def user_data(self, uid):
        user = requests.get("https://api.instagram.com/v1/users/%s/?access_token=%s"%(uid, MAIN_TOKEN))
        if user.json()['meta']['code'] != 200:
            err = "This account is a private account"
            return err
        user_info = user.json()['data']
        return user_info

    def user_media_data(self, uid):
        media = requests.get("https://api.instagram.com/v1/users/%s/media/recent/?access_token=%s"%(uid, MAIN_TOKEN))
        media_data = media.json()['data']
        likes = []
        comments = []
        days = []
        hours = []
        filters = []
        locations = []
        date_range = []

        # create date range 
        date_range.append(datetime.datetime.fromtimestamp(int(media_data[0]['created_time'])).strftime('%b %e/%y'))
        date_range.append(datetime.datetime.fromtimestamp(int(media_data[len(media_data)-1]['created_time'])).strftime('%b %e/%y'))
        
        #create a loop to get the relevant data
        for i in range(0, len(media_data)):
            try:
                likes.append(media_data[i]['likes']['count'])
                comments.append(media_data[i]['comments']['count'])
                days.append(datetime.datetime.fromtimestamp(int(media_data[i]['created_time'])).strftime('%a'))
                hours.append(datetime.datetime.fromtimestamp(int(media_data[i]['created_time'])).strftime('%H'))
                filters.append(media_data[i]['filter'])
                locations.append([media_data[i]['location']['latitude'], media_data[i]['location']['longitude']])
            except (KeyError, TypeError, IndexError) as e:
                pass

            days_pos = [["Mon", 0], ["Tue", 0], ["Wed", 0], ["Thu", 0], ["Fri", 0], ["Sat", 0], ["Sun", 0]]

            for i in range(0, len(days)):
                for j in range(0, len(days_pos)):
                    if days[i] in days_pos[j][0]:
                        days_pos[j][1] = days_pos[j][1]+1

            hours_pos = [["00", 0], ["01", 0], ["02", 0], ["03", 0], ["04", 0], ["05", 0],
            ["06", 0], ["07", 0], ["08", 0], ["09", 0], ["10", 0], ["11", 0],
            ["12", 0], ["13", 0], ["14", 0], ["15", 0], ["16", 0], ["17", 0],
            ["18", 0], ["19", 0], ["20", 0], ["21", 0], ["22", 0], ["23", 0],
            ]

            for i in range(0, len(hours)):
                for j in range(0, len(hours_pos)):
                    if hours[i] in hours_pos[j][0]:
                        hours_pos[j][1] = hours_pos[j][1]+1

            filters_arr = []
            filter_items = dict(Counter(filters)).items()
            for key, value in filter_items:
                temp = [str(key), value]
                filters_arr.append(temp)
            all_data = {
                'likes': list(reversed(likes)),
                'comments': list(reversed(comments)),
                'days': days_pos,
                'hours': hours_pos,
                'filters': filters_arr,
                'locations': locations,
                'date_range': date_range 
            }
        return all_data

    def get_followers(self, uid):
        user_request = requests.get("https://api.instagram.com/v1/users/%s/?access_token=%s" % (uid, BACK_UP_CLIENT))
        # followers = self.api.user_recent_media(uid=uid, count=5)
        user_info = user_request.json()['data']
    
    def get_user_media(self, uid):
        media_request = requests.get("https://api.instagram.com/v1/users/%s/media/recent/?access_token=%s&count=33" % (uid, BACK_UP_CLIENT))

        media = media_request.json()['data']

def main():
    api = instagramClient(BACK_UP_CLIENT)
    user = api.get_user_media(uid = '349813513')


def get_token(user, provider):
    if (provider == 'instagram'):
        social = user.social_auth.get(provider=provider)
        if (social.extra_data['auth_time']) <= int(time.time()):
            strategy = load_strategy()
            social.refresh_token(strategy)    
    else:
        social = user.social_auth.get(provider=provider)
        if (social.extra_data['auth_time'] + social.extra_data['expires']) <= int(time.time()):
            strategy = load_strategy()
            social.refresh_token(strategy)
    return social.extra_data['access_token']


if __name__ == '__main__':
    main()
        
        
           

