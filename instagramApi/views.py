from django.shortcuts import render
from django.http import JsonResponse
from .utils import get_token, instagramClient
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required()
def analytics(requests):
    template_name = 'instagram/instagram.html'
    context = {

    }
    return render(requests, template_name, context )

@login_required()
def posts(requests):
    access_token = requests.user.social_auth.get(provider ='instagram')
    api = instagramClient(ACCESS_TOKEN = access_token.extra_data['access_token'])
    user_data = api.get_followers(uid = access_token.extra_data['access_token'])
    return JsonResponse(access_token.extra_data['access_token'] , safe=False)
