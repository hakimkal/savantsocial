from savantsocial.settings.base import *

import os



# comment the below when running in production



GOOGLE_MAPS_API_KEY = 'AIzaSyDuo3Q-LQeKHqaPVraJkzxlMns8cWHA1LA'


  # Absolute path to the media directory

SOCIAL_AUTH_INSTAGRAM_KEY = 'ef959ed7776d44eaac39c6b292610243'
SOCIAL_AUTH_INSTAGRAM_SECRET = 'f76bf798d0c34fe9b42baf84b067609b'
SOCIAL_AUTH_INSTAGRAM_AUTH_EXTRA_ARGUMENTS = {'scope': 'likes comments relationships public_content follower_list'}


# This is for development
SOCIAL_AUTH_TWITTER_KEY = 'CBgHGDZr5eQMScky7gM7JL415'
SOCIAL_AUTH_TWITTER_SECRET = 'iv3cxuuOjR3H0Yp07RVLpx9KyVX83es2ZIP5Hx81UTRMqqOP5c'

SOCIAL_AUTH_FACEBOOK_KEY = '751789885019818'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = '7ff1e511fa06ed1c05f27a358edad8a5'  # App Secret


CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

HUEY = {
    'name': DBNAME,  # Use db name for huey.
    'result_store': True,  # Store return values of tasks.
    'events': True,  # Consumer emits events allowing real-time monitoring.
    'store_none': False,  # If a task returns None, do not save to results.
    'always_eager': False,  # If DEBUG=True, run synchronously.
    'store_errors': True,  # Store error info if task throws exception.
    'blocking': False,  # Poll the queue rather than do blocking pop.
    'backend_class': 'huey.RedisHuey',  # Use path to redis huey by default,
    'connection': {
        'host': 'localhost',
        'port': 6379,
        'db': 0,
        'connection_pool': None,  # Definitely you should use pooling!
        # ... tons of other options, see redis-py for details.

        # huey-specific connection parameters.
        'read_timeout': 1,  # If not polling (blocking pop), use timeout.
        'max_errors': 1000,  # Only store the 1000 most recent errors.
        'url': None,  # Allow Redis config via a DSN.
    },
    'consumer': {
        'loglevel': logging.DEBUG,
        'workers': 1,
        'worker_type': 'thread',
        'initial_delay': 0.1,  # Smallest polling interval, same as -d.
        'backoff': 1.15,  # Exponential backoff using this rate, -b.
        'max_delay': 10.0,  # Max possible polling interval, -m.
        'utc': True,  # Treat ETAs and schedules as UTC datetimes.
        'scheduler_interval': 1,  # Check schedule every second, -s.
        'periodic': True,  # Enable crontab feature.
        'check_worker_health': True,  # Enable worker health checks.
        'health_check_interval': 1,  # Check worker health every second.
    },
}