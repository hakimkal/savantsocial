
creds = 'ya29.GluJBRvRz2mn5-xipNYgM7_U6zmzHpAIIPdS2sdTDPRpcZf4b5T2iVSxLyCETLxqBjYceOk3upvtkJR0tQAm4W6QQmo6lXF9MGj3xP4UZPIzlyQ7xIGUM1WK21Vw'


import google.oauth2.credentials
from googleapiclient.discovery import build


def youtube_search(options, creds):
    credentials = google.oauth2.credentials.Credentials(creds)
    youtube = build('youtube', 'v3', credentials=credentials)
  # Call the search.list method to retrieve results matching the specified
  # query term.
    search_response = youtube.search().list(
        q=options['q'],
        part='id,snippet',
        maxResults=options['max_results']
    ).execute()

    videos = []
    channels = []
    playlists = []

    # Add each result to the appropriate list, and then display the lists of
    # matching videos, channels, and playlists.
    for search_result in search_response.get('items', []):
        if search_result['id']['kind'] == 'youtube#video':
            videos.append('%s (%s)' % (search_result['snippet']['title'],
                                    search_result['id']['videoId']))
        elif search_result['id']['kind'] == 'youtube#channel':
            channels.append('%s (%s)' % (search_result['snippet']['title'],
                                    search_result['id']['channelId']))
        elif search_result['id']['kind'] == 'youtube#playlist':
            playlists.append('%s (%s)' % (search_result['snippet']['title'],
                                        search_result['id']['playlistId']))

    print('Videos:\n', '\n'.join(videos), '\n')
    print('Channels:\n', '\n'.join(channels), '\n')
    print('Playlists:\n', '\n'.join(playlists), '\n')


def run_analytics_report(creds):
    credentials = google.oauth2.credentials.Credentials(creds)
    youtube_analytics = build('youtubeAnalytics', 'v1', credentials=credentials)
        # Call the Analytics API to retrieve a report. Pass args in as keyword
    # arguments to set values for the following parameters:
    #
    #   * ids
    #   * metrics
    #   * dimensions
    #   * filters
    #   * start_date
    #   * end_date
    #   * sort
    #   * max_results
    #
    # For a list of available reports, see:
    # https://developers.google.com/youtube/analytics/v1/channel_reports
    # https://developers.google.com/youtube/analytics/v1/content_owner_reports
    analytics_query_response = youtube_analytics.reports().query(
        ids="channel==UCQWFtMZHhR5u7DwUBcorD0Q",
        start_date='2013-02-23',
        end_date='2018-03-23',
        metrics='views,likes,dislikes,'
    ).execute()

    #print 'Analytics Data for Channel %s' % channel_id

    for column_header in analytics_query_response.get('columnHeaders', []):
        print('%-20s' % column_header['name']),
    print

    for row in analytics_query_response.get('rows', []):
        for value in row:
            print('%-20s' % value),
        print


def channels_list_by_username(client, **kwargs):
    response = client.channels().list(
    part = 'contentDetails,snippet'
    ).execute()

    return response

def list_video_localizations(youtube, args):
    results = youtube.videos().list(
        part='snippet,localizations',
        id=args.video_id
    ).execute()

    if 'localizations' in results['items'][0]:
        localizations = results['items'][0]['localizations']
        
        for language, localization in localizations.iteritems():
            print ('Video title is \'%s\' and description is \'%s\' in language \'%s\''
                    % (localization['title'], localization['description'], language))
    else:
        print ('There aren\'t any localizations for this video yet.')





if __name__ == '__main__':
    # youtube_search({'q':'programming', 'max_results':10})
    run_analytics_report(creds)