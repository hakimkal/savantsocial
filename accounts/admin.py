from django.contrib import admin
from .models import Profile, Competitor, RelatedCompany
# Register your models here.


admin.site.register(Profile)
admin.site.register(Competitor)
admin.site.register(RelatedCompany)