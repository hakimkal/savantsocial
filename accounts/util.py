from collections import Counter
import time

def get_hashtags(tweet):
	entities = tweet.entities
	hashtags = entities['hashtags']
	return [tag['text'].lower() for tag in hashtags]


def convert_time(tweet_date):
	ts = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(str(tweet_date),'%a %b %d %H:%M:%S +0000 %Y'))
	return ts