from django.contrib import admin
from django.urls import path, include, re_path
from . import views
from accounts.views import registration_view, activation_view, login_view, logout_view, reset, reset_confirm
from django.views.decorators.cache import cache_page

from django.contrib.auth import views as auth_views

app_name = 'accounts'
urlpatterns = [
    path('competitors/twitter', views.save_twitter_competitor_data, name='add-competitor-twitter'),
    path('competitors/analytics', views.competitors_analytics, name= 'competitor-analytics'),
    path('competitors/delete/<id>', views.delete_competitor, name='delete-competitor'),
    path('competitor/<str:name>/reach', views.competitors_data, name='competitors-data'),
    path('competitor/<id>/reach/visualization', views.reach_visualization, name='competitors-reach'),
    path('competitor/<id>/location/visualization', cache_page(60*15)(views.location_visualization), name='competitors-location'),
    path('competitor/<id>/sentiment/visualization', cache_page(60*15)(views.sentiment_visualization), name='competitors-sentiment'),
    path('competitor/<id>/topics/visualization', cache_page(60*15)(views.topics_visualization), name='competitors-topics'),
    path('brand', views.brand, name='account-brand'),
    path('competitor/<id>/visualization/barchart', views.barchart_visualization, name='competitors-barchart'),
    path('', views.home, name='accounts-home'),
    path('competitors/twitter', views.twitter_competitor, name='add-competitor-twitter'),
    path('', views.home, name='home'),
    path('settings', views.settings_view, name='profile-settings'),
    path('upgrade', views.upgrade_plan, name='upgrade-plan'),
    re_path(r'^logout/$', logout_view, name='logout'),
    re_path(r'^login/$', login_view, name='login'),

    re_path(r'^register/$', registration_view, name='register'),

    path('activate/(?P<activation_key>\w+)', activation_view, name='activation_view'),
    path('reset/<uidb64>/<token>/',
         reset_confirm, name='reset_confirm'),
    path('reset/', reset, name='reset'),

    # Change Password URLs for loggedin users:
    path('password_change',
         auth_views.password_change,
         {'post_change_redirect': '/accounts/password_change/done/'},
         name="password_change"),
    path('password_change/done',
         auth_views.password_change_done)

]
