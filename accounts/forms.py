from django.forms import ModelForm
from .models import Competitor
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.forms.models import inlineformset_factory,modelformset_factory
from django.core.exceptions import NON_FIELD_ERRORS
from django.contrib.auth.models import User
from .models import Profile



class CompetitorTwitterForm(ModelForm):
    class Meta:
        model = Competitor
        fields = ['name', 'description', 'website', 'twitter_id']


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean_username(self):
        username = self.cleaned_data.get("username")
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError("Are you sure you are registered? We cannot find this user.")
        return username

    def clean_password(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        try:
            user = User.objects.get(username=username)
        except:
            user = None
        if user is not None and not user.check_password(password):
            raise forms.ValidationError("Invalid Password")
        elif user is None:
            pass
        else:
            return password

class UserProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ('user',)

        widgets = {

            'user_type': forms.RadioSelect({'empty_value':False})
        }

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = '__all__'
        exclude = ('password','last_login','date_joined','username',)



radchoices =(
(True,'Yes'),
(False,'No')
)

class RegistrationForm(forms.ModelForm):
    email = forms.EmailField(label='Your Email')
    password1 = forms.CharField(label='Password', \
                    widget=forms.PasswordInput())
    password2 = forms.CharField(label='Password Confirmation', \
                    widget=forms.PasswordInput())
    code = forms.CharField(max_length=10,required=False)

    accept_terms = forms.BooleanField(widget=forms.CheckboxInput())

    class Meta:
        model = User
        fields = ['email','first_name','last_name','username']
        error_messages = {
           NON_FIELD_ERRORS: {
               'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
           }}


    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords do not match")
        return password2

    def clean_email(self):
        email = self.cleaned_data.get("email")
        user_count = User.objects.filter(email=email).count()
        if user_count > 0:
            raise forms.ValidationError("This email has already been taken. Please use a different email.")
        return email


    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])

        if commit:
            user.save()
        return user
