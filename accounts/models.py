from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

from django.core.mail import send_mail, EmailMessage
from django.template import Context
from django.urls import reverse

from datetime import datetime
from django.template.loader import render_to_string, get_template


user_types= (
    ('customer','Customer'),
    ('staff','staff'),
)


class EmailConfirmed(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default = 1)
    activation_key = models.CharField(max_length=200)
    confirmed = models.BooleanField(default=False)

    def __unicode__(self):
        vdr = Profile.objects.filter(user=self.user)
        if vdr:
            vendor = vdr[0].user_type
        else:
            vendor = "Admin"
        if self.confirmed == False:
            cnf = vendor + ": Not activated"
        else:
            cnf = vendor + ": Activated"
        theuser = str(self.user.get_full_name()) + " [ " +  str(cnf) + " ]"
        return str(theuser)

    def activate_user_email(self):
        #send email here & render a string
        activation_url = "%s%s" %(settings.SITE_URL, reverse("accounts:activation_view", args=[self.activation_key]))
        context = {
            "activation_key": self.activation_key,
            "activation_url": activation_url,
            "user": self.user.username
        }
        message = render_to_string("accounts/activation_message.txt", context)
        subject = "Activate your Health Intell Account"
        self.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)


    def email_user(self, subject, message, from_email=None, **kwargs):
        try:
            vdr = Profile.objects.filter(user=self.user)


            cc_list = settings.SUPPORT_EMAILS
            #send_mail(subject, message, from_email, [self.user.email], kwargs)
            activation_url = "%s%s" %(settings.SITE_URL, reverse("accounts:activation_view", args=[self.activation_key]))

            ctx = {
            "activation_key": self.activation_key,
            "activation_url": activation_url,
            "user": self.user.get_full_name(),
            'home_url': '%s%s' %(settings.SITE_URL,reverse('home')),
            'login_url': '%s%s' %(settings.SITE_URL,reverse('accounts:login')),
            "vendor_home_url" : '%s%s' %(settings.SITE_URL,reverse('vendor-home')),
            'site_logo_url': "%s%s" %(settings.STATIC_URL,"img/min-logo.png"),
            'email':self.user.email,
            'customer_care_email': "%s" %(settings.CC_EMAIL),
            'customer_care_phone': "%s" %(settings.CC_PHONE),
            'site_name': "%s" %(settings.SITE_NAME),
            'name': self.user.get_full_name(),
            'subject':subject}
            #print self.user.usertype
            if vdr and vdr[0].user_type == 'seller':
                ctx['subject'] = "Activate your Health Intell Account "
                message = get_template('emails/english/account_new_seller.html').render(Context(ctx))
            else:
                ctx['subject'] = "Activate your Health Intell  Account "

                message = get_template('emails/english/account_new_buyer.html').render(Context(ctx))
            new_subject = ctx['subject']
            msg = EmailMessage(new_subject,message, to=[self.user.email], from_email=settings.DEFAULT_FROM_EMAIL,bcc=cc_list,  headers = {'Reply-To': self.user.email })
            msg.content_subtype = 'html'
            msg.send()
        except Exception as e:
            raise e


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default = 1)
    twitter = models.CharField(max_length=255, blank=True, null=True)
    facebook = models.CharField(max_length=255, blank=True, null=True)
    instagram = models.CharField(max_length=255, blank=True, null=True)
    user_type = models.CharField("Signup  As", max_length=20, default='customer', choices=user_types)


    def __unicode__(self):
        return str(self.user_type)


class Competitor(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    website = models.CharField(max_length=255, blank=True, null=True)
    twitter_id = models.CharField(max_length=255, blank=True, null=True)
    facebook_id = models.CharField(max_length=255, blank=True, null=True)
    instagram_id = models.CharField(max_length=255, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('accounts:competitors-data', kwargs={'name': self.twitter_id})



class RelatedCompany(models.Model):
    competitor = models.ForeignKey(Competitor, on_delete=models.CASCADE, default = 1)
    name = models.CharField(max_length=255)