
from .forms import CompetitorTwitterForm
from django.http import JsonResponse
from .models import Competitor
from django.contrib import messages
from twitterApi.forms import SearchForm
from twitterApi.tasks import save_data, get_competitors_data
from django.shortcuts import get_object_or_404
from django.conf import settings
import googlemaps
import time
import re
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.shortcuts import render, HttpResponseRedirect, Http404
from django.contrib.auth import logout, login, authenticate
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .util import get_hashtags
from collections import Counter
from django.urls import reverse
import math
from django.conf import settings
from django.contrib.auth.views import password_reset, password_reset_confirm
from accounts.models import EmailConfirmed, Profile
from twitterApi.sentiment import get_tweet_sentiment
from accounts.forms import LoginForm,   RegistrationForm,   UserForm, UserProfileForm
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from twitterApi.utils import extract_location_temp, TwiiterClient
from twitterApi.views import get_token
from collections import Counter


# CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIM)

@login_required()
def home(request):
    template_name = 'accounts/add-profile.html'
    return render(request, template_name)

@login_required()
def brand(request):
    template_name = 'accounts/brand-name.html'
    return render(request, template_name)

@login_required()  
def save_twitter_competitor_data(request):
    if request.method == 'POST':
        form = CompetitorTwitterForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            desc = form.cleaned_data['description']
            webiste = form.cleaned_data['website']
            id_str = form.cleaned_data['twitter_id']
            try:
                save_data = Competitor(
                    user = request.user,
                    name = name,
                    description = desc,
                    website = webiste,
                    twitter_id = id_str
                )
                save_data.save()
                get_competitors_data(user_id = request.user.id, competitor_twitter_id = id_str)
                messages.success(request, "Successfully saved this brand's data")
            except (TypeError, IndexError, IndentationError) as e:
                messages.warning(request, 'The search query did not match any twitter user')
    form = SearchForm()
    return render(request, 'twitter/twitter-results.html', {'form': form})

@login_required()
def competitors_data(request, name):
    template_name = 'accounts/reach.html'
    username = request.user.competitor_set.get(twitter_id = name)
    return render(request, template_name, {'competitor': username})

@login_required()
def competitors_analytics(request):
    template_name = 'competitor/loading.html'
    user_id = request.user.id
    twitter_data = save_data(user_id)
    return render(request, template_name, {'user_id': user_id})

@login_required()
def reach_visualization(request, id):
    template_name = 'accounts/reach-visualization.html'
    competitor = get_object_or_404(request.user.competitor_set, pk=id)
    return render(request, template_name, {'competitor': competitor})

def location_visualization(request, id):
    template_name = 'accounts/map.html'
    competitor = get_object_or_404(request.user.competitor_set, pk=id)
    twitter_id = competitor.twitter_id
    try:
        access_token = get_token(request.user, provider='twitter')
        gmaps = googlemaps.Client(key=settings.GOOGLE_MAPS_API_KEY)
        api = TwiiterClient(
            access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
        tf = Counter()
        for f in competitor.followersdata_set.all():
            tokens = extract_location_temp(location=f.location)
            tf.update(tokens)
        tags, counts = [], []
        for tag, count in tf.most_common():
            result = gmaps.geocode(tag)
            try:
                tags.append({'lng':result[0]['geometry']['location']['lng'],'lat': result[0]['geometry']['location']['lat']})
                counts.append(count)
            except (IndexError, AttributeError) as e:
                pass            
        
        return render(request, template_name, {
            'competitor': competitor,
            'count': counts,
            'tags': tags
            })
    except Exception as e:
        print('CALLING FROM THIS FUNCTION {}'.format(e))
        return render(request, template_name, {'competitor': competitor})
    return render(request, template_name, {'competitor': competitor})


@login_required()
def sentiment_visualization(request, id):
    analysis = []
    competitor = get_object_or_404(request.user.competitor_set, pk=id)
    access_token = get_token(request.user, provider='twitter')
    api = TwiiterClient(access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])
    template_name = 'accounts/linechart.html'
    context = {
        'competitor': competitor
    }
    try:
        user = api.get_user_data(query=competitor.twitter_id)
        tweets = api.search_tweets(q=user._json['screen_name'])
        print(tweets[0])
        if len(tweets) > 0:
            for tweet in tweets:
                sentiment = get_tweet_sentiment(tweet.text)
                analysis.append(sentiment)
            positive = len([x for x in analysis if x['value'] > 0]) / len(analysis)
            negative = len([x for x in analysis if x['value'] < 0]) / len(analysis)
            neutral = len([x for x in analysis if x['value'] == 0]) / len(analysis)
        context = {
            'competitor': competitor,
            'analysis': analysis,
            'positive': positive,
            'negative': negative,
            'neutral': neutral
        }
        return render(request, template_name, context)
    except Exception as e:
        print(e)
        messages.warning(request, 'Unable to analize your data')
    return render(request, template_name, context)


@login_required()
def topics_visualization(request, id):
    template_name = 'accounts/Listchart.html'
    hashtags = Counter()
    competitor = get_object_or_404(request.user.competitor_set, pk=id)
    context = {
         'competitor': competitor
        }
    try:
        access_token = get_token(request.user, provider='twitter')
        api = TwiiterClient(access_token=access_token['oauth_token'], access_token_secret=access_token['oauth_token_secret'])

        user = api.get_user_data(query=competitor.twitter_id)
        tweets = api.search_tweets(q=user._json['screen_name'])
        tags, counts, add = [], [], 0
        if len(tweets) > 0:
            for tweet in tweets:
                hashtags_in_tweet = get_hashtags(tweet)
                hashtags.update(hashtags_in_tweet)
            for tag, count in hashtags.most_common(12):
                print('{}: {}'.format(tag, count))
                tags.append(tag)
                counts.append(count)
            for c in counts:
                add += int(c)
            percent = [math.ceil((x/add)*100) for x in counts]
        topics = [{'tag': x, 'count': y} for x in tags for y in percent]
        print(topics)
        context = {
         'competitor': competitor,
         'topics': topics,
        }
        print('and we are here')
        return render(request, template_name, context)
    except Exception as e:
        print(e)
        return render(request, template_name, context)



@login_required()
def barchart_visualization(request, id):
    template_name = 'accounts/barchart.html'
    competitor = get_object_or_404(request.user.competitor_set, pk=id)
    return render(request, template_name, {'competitor': competitor})


def twitter_competitor(request):
    pass


def logout_view(request):
    logout(request)
    messages.success(request, "Successfully Logged out. Feel free to login again.", extra_tags='')
    messages.warning(request, "There's a warning.")
    messages.error(request, "There's an error.")
    return HttpResponseRedirect('%s' % (reverse("accounts:login")))


def login_view(request):
    form = LoginForm(request.POST or None)
    btn = "Login"

    if request.GET.get('next') is not None :
        messages.error(request, "You must  be logged-in to continue...")
        redirect_to = request.GET.get('next')
        request.session['redirect_to'] = redirect_to
    else:
        redirect_to = ''

    if (request.user.is_authenticated  ):
        return HttpResponseRedirect(reverse("home"))


    elif request.user.is_authenticated and request.user.is_superuser:
        pass

    if request.POST:


        username = request.POST['username']
        password = request.POST['password']
        get_user = False
        if re.match(r"[\w\.\-\+]+@[\w.-]+.\w+", username):
            try:
                get_user = User.objects.get(email=username)

            except ObjectDoesNotExist as e:
                get_user = False
        if get_user:
            username = get_user.username
            user = authenticate(username=username, password=password)
            if user is None or user.is_anonymous:
                messages.error(request, "Invalid username or  password")
                return HttpResponseRedirect(reverse("accounts:login"))

            login(request, user)

            if not request.user.is_authenticated:
                # login(request, user)
                messages.error(request, "Invalid username or  password")
                return HttpResponseRedirect(reverse("accounts:login"))


        else:
            try:
                user = authenticate(username=username, password=password)
                login(request, user)
            except:
                messages.error(request, "Invalid username or  password")

                return HttpResponseRedirect(reverse("accounts:login"))



        if (request.user.is_authenticated  ):
            messages.success(request, "Successfully Logged In. Welcome Back!")
            try:
                redirect_to = request.session['redirect_to']
                if redirect_to != '':
                    return HttpResponseRedirect(redirect_to)
            except:
                pass
            return HttpResponseRedirect(reverse("home"))




        else:
            messages.error(request, "Please confirm your account before you can proceed!")
            logout(request)
            return HttpResponseRedirect(reverse("accounts:login"))

    context = {
        "form": form,
        "submit_btn": btn,
    }

    return render(request, "accounts/login.html", context)


def registration_view(request, user_type=None):
    if request.user.is_authenticated:
        messages.error(request, "You are loggedin as %s, please logout and attempt to register again!" % (
        request.user.get_full_name()))
        return HttpResponseRedirect(reverse("accounts:login"))

    form = RegistrationForm(request.POST or None)

    user_profile = UserProfileForm(request.POST or None)

    context = {
        "form": form,

        "user_profile_form": user_profile,

        "user_type": user_type

    }

    if not form.is_valid() or not user_profile.is_valid():
        return render(request, "accounts/register.html", context)

    if request.method == "POST":
        if form.is_valid() and user_profile.is_valid():
            if form.cleaned_data["accept_terms"] == True:
                new_user = form.save(commit=False)
                new_user.save()
                User = get_user_model()
                n_u = User.objects.get(email=form.cleaned_data["email"])
                new_user_profile = user_profile.save(commit=False)
                new_user_profile.user = n_u
                new_user_profile.save()
                request.session["page_message"] = "Successfully Registered. Please confirm your email now."
                messages.success(request, "Successfully Registered. Please confirm your email now.")

                return HttpResponseRedirect('/')

    return render(request, "accounts/register.html", context)


SHA1_RE = re.compile('^[a-f0-9]{40}$')


def activation_view(request, activation_key):
    if SHA1_RE.search(activation_key):
        try:
            instance = EmailConfirmed.objects.get(activation_key=activation_key)
        except EmailConfirmed.DoesNotExist:
            instance = None
            messages.success(request, "There was an error with your request.")
            return HttpResponseRedirect("/")
        if instance is not None and not instance.confirmed:
            page_message = "Confirmation Successful! Welcome."
            instance.confirmed = True
            instance.activation_key = "Confirmed"
            instance.save()
            messages.success(request, "Successfully Confirmed! Please login.")
            return HttpResponseRedirect(reverse('accounts:login'))
        elif instance is not None and instance.confirmed:
            page_message = "Already Confirmed"
            messages.success(request, "Already Confirmed.")
            return HttpResponseRedirect(reverse('vendor-home'))
        else:
            page_message = ""
            messages.success(request, "Invalid activation link, Please register again.")

        context = {"page_message": page_message}
        return render(request, "accounts/activation_complete.html", context)
    else:
        raise Http404

def reset_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(request, template_name='accounts/reset_confirm.html',
                                  uidb64=uidb64, token=token, post_reset_redirect=reverse('accounts:login'))


def reset(request):
    return password_reset(request, template_name='accounts/change_pass_public_2.html',
                          email_template_name='accounts/reset_email.html',
                          subject_template_name='accounts/reset_subject.txt',
                          post_reset_redirect="%s" % (reverse('accounts:login')),
                          from_email=settings.DEFAULT_FROM_EMAIL,
                          html_email_template_name='accounts/reset_email.html')




def settings_view(request):
    template_name = 'accounts/settings.html'
    return render(request, template_name, {} )

def delete_competitor(request, id):
    template_name = 'accounts/settings.html'
    competitor = get_object_or_404(Competitor, id=id)
    if request.method == 'POST':
        competitor.delete()
        messages.success(request, 'Successfully Deleted')
    else:
        return JsonResponse({'data': 'Cannot perform a get request'})
    return render(request, template_name, {})



def upgrade_plan(request):
    template_name = 'accounts/upgrade-plan.html'
    return render(request, template_name, {})